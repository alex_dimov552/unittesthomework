package com.endava.internship.adimov;

import java.util.*;

class Node {

    Student data;
    Node left;
    Node right;

    public Node(Student data) {
        this.data = data;
        left = null;
        right = null;
    }

}

public class StudentTreeSet implements Set<Student> {
    private Stack<Student> stack = new Stack<>();
    private Node root;

    Student[] array;

    public StudentTreeSet() {
        this.root = null;
    }

    @Override
    public boolean add(Student studentData) {
        Node newNode = new Node(studentData);
        if (root == null) {
            root = newNode;
            return true;
        }
        Node current = root;
        Node parrent = null;

        while (true) {
            parrent = current;
            if (current.data.compareTo(studentData) < 0) {
                current = current.left;
                {
                    if (current == null) {
                        parrent.left = newNode;
                        return true;
                    }
                }

            } else if (current.data.compareTo(studentData) > 0) {

                current = current.right;
                {
                    if (current == null) {
                        parrent.right = newNode;
                        return true;
                    }
                }
            } else
                return false;
        }
    }

    private void displayTree(Node root) {
        if (root != null) {
            displayTree(root.left);
            System.out.println(root.data);
            displayTree(root.right);
        }
    }

    public void display() {
        if (root != null) {
            displayTree(root.left);
            System.out.println(root.data);
            displayTree(root.right);
        }
    }

    private void pushStudent(Node root) {
        if (root != null) {
            pushStudent(root.left);
            stack.push(root.data);
            pushStudent(root.right);
        }
    }

    public void push() {
        if (root != null) {
            pushStudent(root.left);
            stack.push(root.data);
            pushStudent(root.right);
        }
    }

    public boolean hasNext() {
        if (stack.isEmpty()) {
            return false;
        }
        return true;
    }

    public Student next() {
        if (hasNext()) {
            Student a = stack.pop();
            System.out.println(a);
            return a;
        } else {
            return null;
        }

    }

    @Override
    public Iterator<Student> iterator() {
        Student student = next();
        return (Iterator<Student>) student;
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node node) {
        if (node == null)
            return 0;
        else
            return (size(node.left) + 1 + size(node.right));
    }

    @Override
    public boolean isEmpty() {
        if (root == null)
            return true;
        return false;
    }

    @Override
    public boolean contains(Object o) {
        Node current = root;
        while (current != null) {
            if (current.data.compareTo((Student) o) == 0) {
                return true;
            } else if (current.data.compareTo((Student) o) < 0) {
                current = current.left;
            } else
                current = current.right;
        }
        return false;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        array = new Student[size()];
        populateArray(this.root);
        return (T[]) this.array;
    }

    @Override
    public boolean remove(Object o) {
        Node current = root;
        Node parrent = root;
        boolean isLeftChild = false;
        while (current.data.compareTo((Student) o) != 0) {
            parrent = current;
            if (current.data.compareTo((Student) o) < 0) {
                isLeftChild = true;
                current = current.left;
            } else if (current.data.compareTo((Student) o) > 0) {
                isLeftChild = false;
                current = current.right;
            }
            if (current == null) {
                return false;
            }
        }
        if ((current.left == null) && (current.right == null)) {
            if (current == root) {
                root = null;
            } else if (isLeftChild == true) {
                parrent.left = null;
            } else {
                parrent.right = null;
            }
        } else if (current.right == null) {
            if (current == root) {
                root = current.left;
            } else if (isLeftChild) {
                parrent.left = current.left;
            } else {
                parrent.right = current.left;
            }
        } else if (current.left == null) {
            if (current == root) {
                root = current.right;
            } else if (isLeftChild) {
                parrent.left = current.right;
            } else {
                parrent.right = current.right;
            }
        } else if (current.left != null && current.right != null) {

            Node successor = getSuccessor(current);
            if (current == root) {
                root = successor;
            } else if (isLeftChild) {
                parrent.left = successor;
            } else {
                parrent.right = successor;
            }
            successor.left = current.left;
        }
        return true;
    }

    public Node getSuccessor(Node deleleNode) {
        Node successsor = null;
        Node successsorParent = null;
        Node current = deleleNode.right;
        while (current != null) {
            successsorParent = successsor;
            successsor = current;
            current = current.left;
        }

        if (successsor != deleleNode.right) {
            successsorParent.left = successsor.right;
            successsor.right = deleleNode.right;
        }
        return successsor;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean containsAllResponse = true;
        Iterator<Student> iterator = (Iterator<Student>) c.iterator();
        while (iterator.hasNext()) {
            if (!this.contains(iterator.next())) {
                containsAllResponse = false;
            }
        }
        return containsAllResponse;
    }

    @Override
    public boolean addAll(Collection<? extends Student> c) {
        Iterator<Student> iterator = (Iterator<Student>) c.iterator();

        boolean addFlag = false;
        while (iterator.hasNext()) {
            Student student = iterator.next();

            if (!(this.contains(student))) {
                add(student);
                addFlag = true;
            }
        }
        return addFlag;
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        Iterator<Student> iterator = (Iterator<Student>) c.iterator();
        boolean retainFlag = false;

        while (hasNext()) {
            Student stud1 = next();

            if (!(c.contains(stud1))) {
                retainFlag = true;
                this.remove(stud1);
            }
        }
        return retainFlag;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Iterator<Student> iterator = (Iterator<Student>) c.iterator();
        boolean removalFlag = false;
        while (iterator.hasNext()) {
            Student student = iterator.next();
            if (this.contains(student)) {
                remove(student);
                removalFlag = true;
            }
        }
        return removalFlag;
    }

    @Override
    public void clear() {
        root = null;
    }

    int count = 0;

    public void populateArray(Node root) {
        if (root != null) {
            populateArray(root.left);
            array[count++] = root.data;
            populateArray(root.right);
        }

    }

    @Override
    public Object[] toArray() {
        array = new Student[size()];
        populateArray(this.root);
        return this.array;
    }
}


