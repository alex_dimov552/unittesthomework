package com.endava.internship.adimov;

import java.time.LocalDate;
import java.util.Comparator;

public class Student implements Comparable<Student> {
    private String name;
    private LocalDate dateOfbirth;
    private String details;

    public Student(String name, LocalDate dateOfBirth, String details) {
        this.name = name;
        this.dateOfbirth = dateOfBirth;
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfbirth;
    }

    public String getDetails() {
        return details;
    }

    @Override
    public int compareTo(Student o) {

        return -Comparator.comparing(Student::getName) //compare names
                .thenComparing(Student::getDateOfBirth) // then compare objectDateOfBirth
                .compare(this, o);
    }

    @Override
    public String toString() {
        return "Student [name=" + name + ", dateOfbirth=" + dateOfbirth + ", details=" + details + "]";
    }
}