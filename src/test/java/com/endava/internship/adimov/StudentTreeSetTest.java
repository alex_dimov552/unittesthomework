package com.endava.internship.adimov;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.testng.Assert.*;

class StudentTreeSetTest {

    StudentTreeSet stTree = new StudentTreeSet();

    Student[] student = new Student[7];

    @Test
    void add() {
        Student student1 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        Student student5 = new Student("Balds", LocalDate.of(2016, 7, 12), "NUT");
        Student student3 = new Student("Ragnr", LocalDate.of(2016, 7, 12), "NUT");
        Student student4 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student8 = new Student("Ragnar", LocalDate.of(2015, 7, 12), "NUT");

        boolean res1 = stTree.add(student1);
        boolean res2 = stTree.add(student2);
        boolean res3 = stTree.add(student4);
        boolean res4 = stTree.add(student5);
        boolean res5 = stTree.add(student3);
        boolean res6 = stTree.add(student8);


        assertTrue(stTree.contains(student1));
        assertTrue(res1);
        assertTrue(res2);
        assertTrue(res4);
        assertFalse(res3);
        assertTrue(res5);
        assertTrue(res6);
    }

    @Test
    void displayTree() {

        Student student1 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        Student student3 = new Student("Flint", LocalDate.of(2016, 7, 12), "NUT");
        Student student4 = new Student("Black Beard", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student1);
        stTree.add(student2);
        stTree.add(student3);
        stTree.add(student4);

        stTree.display();

    }

    @Test
    void pushStudent() {

        Student student1 = new Student("Baston", LocalDate.of(2016, 7, 12), "NUT");
        stTree.add(student1);
        stTree.push();

        assertThat(stTree.next().equals(student1));

    }

    @Test
    void hasNext() {
        Student student1 = new Student("Baldur", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        stTree.add(student1);
        stTree.add(student2);
        stTree.push();

        boolean res = stTree.hasNext();

        assertTrue(res);

    }

    @Test
    void next() {
        Student student1 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student1);
        stTree.add(student2);

        stTree.push();

        Student student = stTree.next();
        Student student5 = stTree.next();
        Student student6 = stTree.next();


        assertTrue(student.equals(student2));
        assertTrue(student5.equals(student1));
        assertNull(student6);


    }

    @Test
    void iterator() {
        Student student1 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        stTree.add(student1);
        stTree.add(student2);
        stTree.iterator();
    }

    @Test
    void size() {
        Student student1 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        Student student3 = new Student("Flint", LocalDate.of(2016, 7, 12), "NUT");
        Student student4 = new Student("Black Beard", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student1);
        stTree.add(student2);
        stTree.add(student3);
        stTree.add(student4);

        int size = stTree.size();

        assertTrue(size == 4);

    }

    @Test
    void isEmpty() {
        Student student1 = new Student("Vorador", LocalDate.of(2016, 7, 12), "NUT");
        stTree.add(student1);
        assertFalse(stTree.isEmpty());

        stTree.remove(student1);
        assertTrue(stTree.isEmpty());
    }

    @Test
    void contains() {
        Student student1 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student1);

        boolean rez = stTree.contains(student1);
        boolean rez2 = stTree.contains(student2);

        assertTrue(rez);
        assertFalse(rez2);

    }

    @Test
    void remove() {
        Student student1 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        Student student3 = new Student("aldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student4 = new Student("Ranar", LocalDate.of(2016, 7, 12), "NUT");
        Student student5 = new Student("Bas", LocalDate.of(2016, 7, 12), "NUT");
        Student student6 = new Student("ar", LocalDate.of(2016, 7, 12), "NUT");
        Student student7 = new Student("fgdgdg", LocalDate.of(2016, 7, 12), "NUT");
        Student student8 = new Student("fuygfygr", LocalDate.of(2016, 7, 12), "NUT");
        Student student9 = new Student("fgdgdg", LocalDate.of(2016, 7, 12), "NUT");
        Student student10 = new Student("fuygfygr", LocalDate.of(2016, 7, 12), "NUT");
        Student student11 = new Student("fgdgdg", LocalDate.of(206, 7, 12), "NUT");
        Student student12 = new Student("fuygfygr", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student1);
        stTree.remove(student1);

        stTree.add(student2);
        stTree.add(student3);
        stTree.remove(student3);

        stTree.add(student2);
        stTree.add(student3);
        stTree.add(student4);
        stTree.remove(student4);

        stTree.add(student7);
        stTree.add(student8);
        stTree.add(student9);
        stTree.add(student10);
        stTree.remove(student10);
        stTree.add(student11);
        stTree.add(student12);
        stTree.add(student5);
        stTree.remove(student7);

        stTree.remove(student12);
        stTree.add(student12);

        stTree.remove(student2);
        stTree.add(student2);

        stTree.remove(student6);
        stTree.add(student6);

        stTree.remove(student4);
        stTree.remove(student10);
        stTree.remove(student6);


        assertTrue(stTree.contains(student2));
        assertFalse(stTree.contains(student8));
        assertFalse(stTree.contains(student6));
        assertFalse(stTree.contains(student4));
    }

    @Test
        void containsAll() {
        List<Student> arrayList = new ArrayList<>();

        Student student2 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student8 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        Student student7 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student2);
        stTree.add(student8);

        arrayList.add(student8);
        arrayList.add(student2);
        arrayList.add(student7);

        boolean res = stTree.containsAll(arrayList);

        assertTrue(res);

    }

    @Test
    void addAll() {
        List<Student> arrayList = new ArrayList<>();
        Student student2 = new Student("Baldurs", LocalDate.of(2016, 7, 12), "NUT");
        Student student8 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        Student student85 = new Student("Raar", LocalDate.of(2016, 7, 12), "NUT");
        Student student5 = new Student("Raar", LocalDate.of(201, 7, 12), "NUT");

        stTree.add(student2);
        stTree.add(student8);


        arrayList.add(student8);
        arrayList.add(student2);
        boolean res = stTree.addAll(arrayList);

        arrayList.add(student85);
        arrayList.add(student8);
        boolean res1 = stTree.addAll(arrayList);

        assertTrue(res1);
        assertFalse(res);

    }

    @Test
    void retainAll() {
        List<Student> arrayList = new ArrayList<>();
        Student student2 = new Student("Olga", LocalDate.of(2016, 7, 12), "NUT");
        Student student8 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        Student student5 = new Student("Rodrick", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student2);
        stTree.add(student8);
        stTree.add(student5);
        stTree.push();

        arrayList.add(student8);
        arrayList.add(student2);

        boolean res = stTree.retainAll(arrayList);

        assertTrue(res);
        assertEquals(stTree.size() ,2);

    }

    @Test
    void removeAll() {
        List<Student> arrayList = new ArrayList<>();
        Student student2 = new Student("Siegfreed", LocalDate.of(2016, 7, 12), "NUT");
        Student student8 = new Student("Kael", LocalDate.of(2016, 7, 12), "NUT");
        Student student1 = new Student("Kail", LocalDate.of(2016, 7, 12), "NUT");
        Student student7 = new Student("Kilgan", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student1);
        stTree.add(student2);
        stTree.add(student8);

        arrayList.add(student8);
        arrayList.add(student2);
        boolean res = stTree.removeAll(arrayList);

        arrayList.add(student7);

        boolean res2 = stTree.removeAll(arrayList);

        assertTrue(res);
        assertFalse(res2);

    }

    @Test
    void clear() {
        Student student1 = new Student("", LocalDate.of(2016, 7, 12), "NUT");
        Student student2 = new Student("Ragnar", LocalDate.of(2016, 7, 12), "NUT");
        stTree.add(student1);
        stTree.add(student2);

        stTree.clear();

        assertThat(stTree.isEmpty());

    }

    @Test
    void populateArray() {
        Student student8 = new Student("Kael", LocalDate.of(2016, 7, 12), "NUT");
        Student student1 = new Student("Kratos", LocalDate.of(2016, 7, 12), "NUT");
        Student student6 = new Student("Cronos", LocalDate.of(2016, 7, 12), "NUT");


        student[0] = student1;
        student[1] = student6;
        student[2] = student8;


        assertTrue(stTree.toArray() != null);

    }

    @Test
    void testToArray() {
        Student student8 = new Student("Kael", LocalDate.of(2016, 7, 12), "NUT");
        Student student1 = new Student("Kratos", LocalDate.of(2016, 7, 12), "NUT");

        stTree.add(student1);
        stTree.add(student8);

        assertTrue(stTree.toArray() != null);

    }
}